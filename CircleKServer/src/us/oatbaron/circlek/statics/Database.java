package us.oatbaron.circlek.statics;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;

import us.oatbaron.circlek.objects.Email;
import us.oatbaron.circlek.objects.EmailSender;
import us.oatbaron.circlek.objects.Event;
import us.oatbaron.circlek.objects.Member;

public class Database {
	private static ArrayList<Event> events;
	private static ArrayList<Member> members;
	private static Logger logger;
	private static File rootDir;
	private static File eventsDir;
	private static File membersDir;
	private static File settings;
	
	// Initializes the database by detecting directories and loading files
	public static void init(){
		events = new ArrayList<Event>();
		members = new ArrayList<Member>();
		logger = Logger.getLogger("Circle K Server");
		
		rootDir = new File(System.getProperty("user.dir")+"\\");
		if(!rootDir.exists())
			rootDir.mkdir();
		membersDir = new File(rootDir.getPath()+"\\members");
		if(!membersDir.exists())
			membersDir.mkdir();
		eventsDir = new File(rootDir.getPath()+"\\events");
		if(!eventsDir.exists())
			eventsDir.mkdir();
		settings = new File(rootDir.getPath()+"\\settings.uwpck");
		if(!settings.exists())
			Database.generateDefaultSettings();
		Database.loadSettings(settings);
		
		// Dirty way of referencing cross-files: init load without definitions followed by re-parse adding definitions
		for(int i=0;i<2;i++){
			if(membersDir.listFiles() != null)
				for(File f : membersDir.listFiles())
					if(!Database.loadMember(f))
						Database.warn("Could not load "+f.getPath()+"\\"+f.getName());
			if(eventsDir.listFiles() != null)
				for(File f : eventsDir.listFiles())
					if(!Database.loadEvent(f))
						Database.warn("Could not load "+f.getPath()+"\\"+f.getName());
		}
		
		save(members.get(0));
		save(events.get(0));
		
		Email email = new Email();
		email.address = "ctnelson1997@gmail.com";
		email.subject = "test run uwp-cki util";
		email.body = "Testing UWP-CKI util\n @ver=v4.0a\n @author=cnelson\n @date 12/29/2016";
		
		//new Thread(new EmailSender(email)).start();
		//new Thread(new EmailSender(events.get(0))).start();
	}
	
	// Logs a standard message
	public static void log(String message){
		logger.log(Level.INFO, message);
	}
	
	// Logs a warning message
	public static void warn(String message){
		logger.log(Level.WARNING, message);
	}
	
	// Default settings, saved to disk
	public static boolean generateDefaultSettings(){
		try (PrintStream out = new PrintStream(new FileOutputStream(settings))) {
			out.println("oostburgkeyclub@gmail.com");
			out.println("smtp.gmail.com");
			out.println("587");
			out.println(Encrypter.encrypt("123456"));
			out.print(Encrypter.encrypt("123456"));
			out.close();
			return true;
		} catch(Exception err) {
			err.printStackTrace();
			return false;
		}
	}
	
	// Load settings from disk at settings.uwpcrk (UW-Platteville Circle K)
	public static boolean loadSettings(File f){
		String strFile = Database.getFileContents(f);
		if(strFile == null)
			return false;
		String[] contents = strFile.split("\\r?\\n");
		Settings.EMAIL_ACCOUNT = contents[0];
		Settings.SMTP_HOST = contents[1];
		Settings.SMTP_PORT = contents[2];
		Settings.EMAIL_PASSWORD = Encrypter.decrypt(contents[3]);
		Settings.ELEVATED_PASSWORD = Encrypter.decrypt(contents[4]);
		return true;
	}
	
	// Loads a Member from a file into an object and into the array
	public static boolean loadMember(File f){
		String strFile = Database.getFileContents(f);
		if(strFile == null)
			return false;
		String[] contents = strFile.split("\\r?\\n");
		String fName = contents[0];
		String lName = contents[1];
		String email = contents[2];
		boolean optout = Boolean.parseBoolean(contents[3]);
		Member m = new Member(email);
		for(int i=4;i<contents.length;i++){
			if(Database.getEvent(UUID.fromString(contents[i])) != null)
				m.addEvent(UUID.fromString(contents[i]));
		}
		m.setFirstName(fName);
		m.setLastName(lName);
		m.setOptout(optout);
		Database.updateMember(m);
		return true;
	}
	
	// Loads an Event from a file into an object and into the array
	public static boolean loadEvent(File f){
		String strFile = Database.getFileContents(f);
		if(strFile == null)
			return false;
		String[] contents = strFile.split("\\r?\\n");
		String name = contents[0];
		String address = contents[1];
		Date startDate = Date.from(Instant.parse(contents[2]));
		Date endDate = Date.from(Instant.parse(contents[3]));
		double hours = Double.parseDouble(contents[4]);
		String info = contents[5].replaceAll(";@!", "\n");
		boolean archived = Boolean.parseBoolean(contents[6]);
		Event event = new Event(UUID.fromString(f.getName().split("\\.")[0]));
		event.setName(name);
		event.setAddress(address);
		event.setStartDate(startDate);
		event.setEndDate(endDate);
		event.setHours(hours);
		event.setInfo(info);
		event.setArchived(archived);
		for(int i=7;i<contents.length;i++)
			if(Database.getMember(contents[i]) != null)
				event.addVolunteer(Database.getMember(contents[i]).getEmail());
		Database.updateEvent(event);
		return true;
	}
	
	// Gets the data from a file in the form of a single string
	private static String getFileContents(File f){
		FileInputStream inputStream;
		try {
			inputStream = new FileInputStream(f);
		} catch (FileNotFoundException e0) {
			e0.printStackTrace();
			return null;
		}
		try {
			return IOUtils.toString(inputStream);
		} catch(Exception e1) {
			e1.printStackTrace();
		} finally {
			try {
				inputStream.close();
			} catch (IOException e2) {
				e2.printStackTrace();
			}
		}
		return null;
	}
	
	// Gets a specified Event from the array
	public static Event getEvent(UUID uuid){
		for(Event e : Database.events)
			if(e.getUuid().equals(uuid))
				return e;
		return null;
	}
	
	// Gets a specified Member from the array
	public static Member getMember(String email){
		for(Member m : Database.members)
			if(m.getEmail().equalsIgnoreCase(email))
				return m;
		return null;
	}

	// Updates an Event (in memory and on disk), usually called from the client.
	public static void updateEvent(Event event) {
		//TODO Save to file
		Event registeredEvent = Database.getEvent(event.getUuid());
		if(registeredEvent != null)
			Database.events.remove(registeredEvent);
		Database.events.add(event);
		Database.save(event);
	}
	
	// Updates a Member (in memory and on disk), usually called from the client.
	public static void updateMember(Member member) {
		Member registeredMember = Database.getMember(member.getEmail());
		if(registeredMember != null)
			Database.members.remove(registeredMember);
		Database.members.add(member);
		Database.save(member);
	}
	
	// Gets a listing of all Members
	public static ArrayList<Member> getMembers(){
		return Database.members;
	}
	
	// Gets a listing of all events
	public static ArrayList<Event> getEvents(){
		return Database.events;
	}

	// Deletes a Member (in memory and on disk)
	public static boolean deleteMember(String email) {
		Member m = Database.getMember(email);
		if(m != null)
			Database.members.remove(m);
		else
			return false;
		return Database.delete(m);
	}
	
	// Deletes an Event (in memory and on disk)
	public static boolean deleteEvent(UUID uuid) {
		Event event = Database.getEvent(uuid);
		if(event != null)
			Database.events.remove(event);
		else
			return false;
		return Database.delete(event);
	}
	
	// Saves a Member to a .uwpckm file (UW-Platteville Circle K Member) using the following format:
	// <first name>
	// <last name>
	// <email>
	// <email optout>
	// <uuid of event attended #1>
	// <uuid event attended #2>
	// <...>
	// <uuid event attended #n>
	public static boolean save(Member m){
		try (PrintStream out = new PrintStream(new FileOutputStream(membersDir.getAbsolutePath()+"\\"+m.getEmail().replaceAll("@", "(at)")+".uwpckm"))) {
			out.println(m.getFirstName());
			out.println(m.getLastName());
			out.println(m.getEmail());
			out.print(m.isOptout());
			for(UUID uuid : m.getEvents()){
				out.println();
				out.print(uuid.toString());
			}
			out.close();
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	// Saves an Event to a .uwpcke file (UW-Platteville Circle K Event) using the following format:
	// <name>
	// <address>
	// <start date (in milliseconds)>
	// <end date (in milliseconds>
	// <hour value>
	// <event info (as a single string)>
	// <archived>
	// <email of member attended #1>
	// <email of member attended #2>
	// <...>
	// <email of member attended #n>
	public static boolean save(Event e){
		try (PrintStream out = new PrintStream(new FileOutputStream(eventsDir.getAbsolutePath()+"\\"+e.getUuid().toString()+".uwpcke"))) {
			out.println(e.getName());
			out.println(e.getAddress());
			out.println(e.getStartDate().toInstant().toString());
			out.println(e.getEndDate().toInstant().toString());
			out.println(e.getHours());
			// could be faulty
			out.println(e.getInfo().replaceAll("\n", ";@!"));
			out.print(e.isArchived());
			for(Member volunteer : e.getVolunteers()){
				out.println();
				out.print(volunteer.getEmail());
			}
			out.close();
			return true;
		} catch(Exception err) {
			err.printStackTrace();
			return false;
		}
	}
	
	public static boolean saveSettings(){
		try (PrintStream out = new PrintStream(new FileOutputStream(settings))) {
			out.println(Settings.EMAIL_ACCOUNT);
			out.println(Settings.SMTP_HOST);
			out.println(Settings.SMTP_PORT);
			out.println(Encrypter.encrypt(Settings.EMAIL_PASSWORD));
			out.print(Encrypter.encrypt(Settings.ELEVATED_PASSWORD));
			out.close();
			return true;
		} catch(Exception err) {
			err.printStackTrace();
			return false;
		}
	}
	
	// Deletes a Member from disk
	public static boolean delete(Member m){
		return new File(rootDir.getAbsolutePath()+"\\"+m.getEmail().replaceAll("@", "(at)")+".uwpckm").delete();
	}
	
	// Deletes an Event from disk
	public static boolean delete(Event e){
		return new File(rootDir.getAbsolutePath()+"\\"+e.getUuid().toString()+".uwpcke").delete();
	}
}
