package us.oatbaron.circlek.statics;

public class Settings {
	public static String EMAIL_ACCOUNT;
	public static String SMTP_HOST;
	public static String SMTP_PORT;
	public static String EMAIL_PASSWORD;
	public static String ELEVATED_PASSWORD;
}
