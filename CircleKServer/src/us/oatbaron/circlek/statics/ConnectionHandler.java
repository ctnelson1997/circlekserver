package us.oatbaron.circlek.statics;

import java.io.IOException;
import java.net.ServerSocket;

import us.oatbaron.circlek.objects.Connection;

public class ConnectionHandler implements Runnable {
	
	private int port = 11525;
	private long clientId = 0;
	
	public void run(){
		ServerSocket socket = null;
		try {
			socket = new ServerSocket(port);
		} catch (IOException e) {
			return;
		}
		try {
			while (true){
				new Thread(new Connection(socket.accept(), clientId++)).start(); 
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	     }
	}
}
