package us.oatbaron.circlek.enums;

import java.io.Serializable;

public enum ElevatedRequest implements Serializable {
	MODIFY_EVENT,
	MODIFY_MEMBER,
	DELETE_EVENT,
	DELETE_MEMBER,
	SET_EMAIL_PASSWORD,
	SET_ELEVATED_PASSWORD,
	SEND_EMAIL,
	SEND_EMAILS,
	SEND_EMAIL_REMINDER,
	WIPE
}
