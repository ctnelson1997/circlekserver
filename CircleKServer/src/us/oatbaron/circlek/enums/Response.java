package us.oatbaron.circlek.enums;

import java.io.Serializable;

public enum Response implements Serializable {
	INVALID,
	ACKNOWLEDGED,
	UNKNOWN_REQUEST,
	UNAUTHORIZED,
	SUCCESS,
	UNDEFINED_FAILURE,
	FAILURE
}
