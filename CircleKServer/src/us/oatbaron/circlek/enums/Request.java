package us.oatbaron.circlek.enums;

import java.io.Serializable;

public enum Request implements Serializable {
	ELEVATE,
	GET_MEMBERS,
	GET_EVENTS,
	MODIFY_SIGNUP
}
