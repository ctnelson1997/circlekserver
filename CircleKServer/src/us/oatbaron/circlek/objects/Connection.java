package us.oatbaron.circlek.objects;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.UUID;

import us.oatbaron.circlek.enums.ElevatedRequest;
import us.oatbaron.circlek.enums.Request;
import us.oatbaron.circlek.enums.Response;
import us.oatbaron.circlek.statics.Database;
import us.oatbaron.circlek.statics.Settings;

public class Connection implements Runnable {
	
	private IOConnection ioconnection;
    private boolean administrative = false;
    
    public Connection(Socket socket, long clientId) {
        ioconnection = new IOConnection(socket, clientId);
        ioconnection.send(clientId);
    }
    
	public void run() {
		while (true) {
        	Object inc = ioconnection.read();
        	if(inc instanceof Request)
        		AssignRequest((Request) inc);
        	else if(inc instanceof ElevatedRequest)
        		if(administrative)
        			AssignElevatedRequest((ElevatedRequest) inc);
        		else
        			ioconnection.send(Response.UNAUTHORIZED);
        	else
        		ioconnection.send(Response.UNKNOWN_REQUEST);
        }
    }
    
    private void AssignRequest(Request req) {
    	ioconnection.send(Response.ACKNOWLEDGED);
    	switch(req){
			case ELEVATE:
				DoElevation();
				break;
			case MODIFY_SIGNUP:
				DoModifySignup();
				break;
			case GET_MEMBERS:
				DoGetMembers();
				break;
			case GET_EVENTS:
				DoGetEvents();
				break;
			default:
				ioconnection.send(Response.UNKNOWN_REQUEST);
				break;
    	}
		
	}
    
	private void AssignElevatedRequest(ElevatedRequest req) {
		ioconnection.send(Response.ACKNOWLEDGED);
    	switch(req){
			case MODIFY_EVENT:
				DoModifyEvent();
				break;
			case MODIFY_MEMBER:
				DoModifyMember();
				break;
			case DELETE_EVENT:
				DoDeleteEvent();
				break;
			case DELETE_MEMBER:
				DoDeleteMember();
				break;
			case SEND_EMAIL:
				DoSendEmail();
				break;
			case SEND_EMAILS:
				DoSendEmails();
				break;
			case SEND_EMAIL_REMINDER:
				DoSendReminderEmail();
				break;
			case SET_EMAIL_PASSWORD:
				DoSetEmailPassword();
				break;
			case SET_ELEVATED_PASSWORD:
				DoSetElevatedPassword();
				break;
			case WIPE:
				DoWipe();
				break;
			default:
				ioconnection.send(Response.UNKNOWN_REQUEST);
				break;
    	}
	}
	
	private void DoModifySignup() {
    	Object inc = ioconnection.read();
		if(!(inc instanceof Member)){
			ioconnection.send(Response.INVALID);
			return;
		}
		
		Event incEvent = (Event) inc;
		Event event = Database.getEvent(incEvent.getUuid());
		if(event == null){
			ioconnection.send(Response.UNDEFINED_FAILURE);
			return;
		}
		
		Member incMember = (Member) inc;
		Member member = Database.getMember(incMember.getEmail());
		if(member == null){
			ioconnection.send(Response.UNDEFINED_FAILURE);
			return;
		}
		
		if(event.getVolunteers().contains(member))
			event.getVolunteers().remove(member);
		else
			event.getVolunteers().add(member);
		
		ioconnection.send(Response.SUCCESS);
	}
    
    private void DoDeleteMember() {
    	Object inc = ioconnection.read();
		if(!(inc instanceof String)){
			ioconnection.send(Response.INVALID);
			return;
		}
		String email = (String) inc;
		if(Database.deleteMember(email))
			ioconnection.send(Response.SUCCESS);
		else
			ioconnection.send(Response.UNDEFINED_FAILURE);
	}

	private void DoDeleteEvent() {
		Object inc = ioconnection.read();
		if(!(inc instanceof UUID)){
			ioconnection.send(Response.INVALID);
			return;
		}
		UUID uuid = (UUID) inc;
		if(Database.deleteEvent(uuid))
			ioconnection.send(Response.SUCCESS);
		else
			ioconnection.send(Response.UNDEFINED_FAILURE);
	}

	private void DoGetMembers() {
		ioconnection.send(Database.getMembers());
	}
    
    private void DoGetEvents() {
		ioconnection.send(Database.getEvents());
	}

	private void DoElevation() {
		Object inc = ioconnection.read();
		if(!(inc instanceof String)){
			ioconnection.send(Response.INVALID);
			return;
		}
		String incPassword = (String) inc;
		if(incPassword.equals(Settings.ELEVATED_PASSWORD)){
			administrative = true;
			ioconnection.send(Response.SUCCESS);
		}else{
			ioconnection.send(Response.FAILURE);
		}
	}
	
	private void DoModifyEvent() {
		Object inc = ioconnection.read();
		if(!(inc instanceof Event)){
			ioconnection.send(Response.INVALID);
			return;
		}
		Event event = (Event) inc;
		Database.updateEvent(event);
		ioconnection.send(Response.SUCCESS);
	}
	
	private void DoModifyMember() {
		Object inc = ioconnection.read();
		if(!(inc instanceof Member)){
			ioconnection.send(Response.INVALID);
			return;
		}
		Member member = (Member) inc;
		Database.updateMember(member);
		ioconnection.send(Response.SUCCESS);
	}

	private void DoSendEmail() {
		Object inc = ioconnection.read();
		if(!(inc instanceof Email)){
			ioconnection.send(Response.INVALID);
			return;
		}
		Email email = (Email) inc;
		new Thread(new EmailSender(email)).start();
		ioconnection.send(Response.SUCCESS);
	}
	
	private void DoSendEmails() {
		Object inc = ioconnection.read();
		if(!(inc instanceof ArrayList<?>)){
			ioconnection.send(Response.INVALID);
			return;
		}
		ArrayList<?> list = (ArrayList<?>) inc;
		for(Object o : list)
			if(!(o instanceof Email)){
				ioconnection.send(Response.INVALID);
				return;
			}
		ArrayList<Email> emails = (ArrayList<Email>) list;
		new Thread(new EmailSender(emails)).start();
		ioconnection.send(Response.SUCCESS);
	}
	
	private void DoSendReminderEmail() {
		Object inc = ioconnection.read();
		if(!(inc instanceof Event)){
			ioconnection.send(Response.INVALID);
			return;
		}
		Event event = (Event) inc;
		new Thread(new EmailSender(event)).start();
		ioconnection.send(Response.SUCCESS);
	}
	
	private void DoSetEmailPassword() {
		Object inc = ioconnection.read();
		if(!(inc instanceof String)){
			ioconnection.send(Response.INVALID);
			return;
		}
		String password = (String) inc;
		Settings.EMAIL_PASSWORD = password;
		if(Database.saveSettings())
			ioconnection.send(Response.SUCCESS);
		else
			ioconnection.send(Response.UNDEFINED_FAILURE);
	}
	
	private void DoSetElevatedPassword() {
		Object inc = ioconnection.read();
		if(!(inc instanceof String)){
			ioconnection.send(Response.INVALID);
			return;
		}
		String password = (String) inc;
		Settings.ELEVATED_PASSWORD = password;
		if(Database.saveSettings())
			ioconnection.send(Response.SUCCESS);
		else
			ioconnection.send(Response.UNDEFINED_FAILURE);
	}
	
	private void DoWipe() {
		boolean success = true;
		for(Member member : Database.getMembers())
			if(!Database.deleteMember(member.getEmail()))
				success = false;
		for(Event event : Database.getEvents())
			if(!Database.deleteEvent(event.getUuid()))
				success = false;
		
		if(success)
			ioconnection.send(Response.SUCCESS);
		else
			ioconnection.send(Response.UNDEFINED_FAILURE);
	}
	
	private class IOConnection {
		
	    private Socket socket;
	    private ObjectInputStream in;
	    private ObjectOutputStream out;
	    private long clientId;
	    
	    public IOConnection(Socket socket, long clientId){
	    	
	    	this.socket = socket;
	    	this.clientId = clientId;
	    	
	        try {
	        	out = new ObjectOutputStream(socket.getOutputStream());
	            in = new ObjectInputStream(socket.getInputStream());
	            out.flush();
	        } catch (Exception e) {
	        	e.printStackTrace();
			}
	    }
		
		private Object read() {
			try{
				if(in.markSupported())
	        		in.reset();
	        	out.reset();
				Object inObj = in.readObject();
				Database.log("[Connection] (Client ID: " + clientId + ") READ: "+inObj.getClass().getName());
	    		Database.log("[Connection]  - Object Info: "+inObj.toString());
				return inObj;
			} catch(Exception e) {
				e.printStackTrace();
				this.closeConnections();
			}
			return null;
		}
		
		private void send(Object outObj) {
			try{
				if(in.markSupported())
	        		in.reset();
	        	out.reset();
				Database.log("[Connection] (Client ID: " + clientId + ") SENT: "+outObj.getClass().getName());
	    		Database.log("[Connection]  - Object Info: "+outObj.toString());
				out.writeObject(outObj);
			} catch(Exception e) {
				e.printStackTrace();
				this.closeConnections();
			}
		}
		
		private void closeConnections() {
			try{
		    	in.close();
				out.close();
				socket.close();
				Thread.currentThread().join();
				Database.log("Connection stopped with Client ID " + clientId);
			} catch(Exception e) {
				e.printStackTrace();
			}
	    }
	}
}