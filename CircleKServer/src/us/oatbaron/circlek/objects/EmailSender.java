package us.oatbaron.circlek.objects;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import us.oatbaron.circlek.statics.Database;
import us.oatbaron.circlek.statics.Settings;

public class EmailSender implements Runnable {
	
	private String address = Settings.EMAIL_ACCOUNT;
	private String password = Settings.EMAIL_PASSWORD;
	private String smtpHost = Settings.SMTP_HOST;
	private String smtpPort = Settings.SMTP_PORT;
	
	private ArrayList<Email> emails = new ArrayList<Email>();
	
	// Puts an email in the "to send" list
	public EmailSender(Email email){    
		emails.add(email);
	}
	
	// Sets the emails in the "to send"
	public EmailSender(ArrayList<Email> emails){
		this.emails = emails;
	}
	
	// Generates and sets the emails for each volunteer in the "to send" list
	public EmailSender(Event event){
		for(Member m : event.getVolunteers()){
			Email email = new Email();
			email.address = m.getEmail();
			email.subject = "UWP Circle K Reminder: "+event.getName();
			String others = "";
			for(Member volunteer : event.getVolunteers()){
				if(volunteer.getEmail().equalsIgnoreCase(m.getEmail()))
					continue;
				others += "\n - " + volunteer.getFirstName() + " " +volunteer.getLastName();
			}
			if(event.getStartDate().getDay() == event.getEndDate().getDay())
				email.body = "Hey "+m.getFirstName()+",\n\n" +
					     "Just a reminder that you are signed up to help at " + event.getName() +
					     " on " + new SimpleDateFormat("yyyy-MM-dd").format(event.getStartDate()) + " from " + new SimpleDateFormat("HH:mm").format(event.getStartDate()) +
					     " until " + new SimpleDateFormat("HH:mm").format(event.getEndDate()) + "\n\n" +
					     "Other people going include: " + others + "\n\n Thank you for volunteering! \n\n\n" +
					     "If you wish to stop recieving these emails, simply reply 'STOP'";
			else
				email.body = "Hey "+m.getFirstName()+",\n\n" +
					     "Just a reminder that you are signed up to help at " + event.getName() +
					     " on " + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(event.getStartDate()) +
					     " until " + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(event.getEndDate()) + "\n\n" +
					     "Other people going include: " + others + "\n\n Thank you for volunteering! \n\n\n" +
					     "If you wish to stop recieving these emails, simply reply 'STOP'";
			
			emails.add(email);
		}
	}
	
	// Sends the emails in the "to send" list on a seperate thread
	public void run(){
		for(Email email : emails){
			if(Database.getMember(email.address) != null && Database.getMember(email.address).isOptout())
				continue;
			Session session = this.generateSession();
			try {
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(address));
				message.setRecipients(RecipientType.TO, InternetAddress.parse(email.address));
				message.setSubject(email.subject);
				message.setText(email.body);
				Transport.send(message);
				Database.log("Sent email '"+email.subject+"' to "+email.address);
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}
	}
	
	// Generates a new email session
	private Session generateSession(){
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", smtpPort);

		return Session.getInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(address, password);
				}
		  	});
	}
}
