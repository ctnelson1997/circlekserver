package us.oatbaron.circlek.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

import us.oatbaron.circlek.statics.Database;

public class Member implements Serializable {

	private static final long serialVersionUID = 6743992622962177387L;
	private String firstName;
	private String lastName;
	private String email;
	private boolean optout;
	private ArrayList<UUID> events;
	
	public Member(String email){
		this.email = email;
		optout = false;
		events = new ArrayList<UUID>();
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isOptout() {
		return optout;
	}

	public void setOptout(boolean optout) {
		this.optout = optout;
	}

	public ArrayList<UUID> getEvents() {
		return events;
	}
	
	public void addEvent(UUID event){
		this.events.add(event);
	}
	
	public void removeEvent(UUID event){
		this.events.remove(event);
	}

	public double getHours(){
		double hours = 0;
		for(UUID event : events)
			hours += Database.getEvent(event).getHours();
		return hours;
	}
	
	public String toString(){
		/*String events = "";
		for(UUID uuid : this.events){
			events+= "\n - "+uuid.toString();
		}*/
		return lastName+"."+firstName+" @ "+email+" ? "+optout;
	}
}
