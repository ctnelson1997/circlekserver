package us.oatbaron.circlek.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import us.oatbaron.circlek.statics.Database;

public class Event implements Serializable {

	private static final long serialVersionUID = 8960822233053421673L;
	private UUID uuid;
	private String name;
	private String address;
	private Date startDate;
	private Date endDate;
	private double hours;
	private String info;
	private boolean archived;
	private ArrayList<String> volunteers;
	
	public Event(UUID uuid){
		this.uuid = uuid;
		
		volunteers = new ArrayList<String>();
		archived = false;
	}
	
	public UUID getUuid() {
		return uuid;
	}
	
	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
		// TODO update hours
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
		// TODO update hours
	}

	public double getHours() {
		return hours;
	}

	public void setHours(double hours) {
		this.hours = hours;
	}

	public String getInfo() {
		return info;
	}
	
	public void setInfo(String info) {
		this.info = info;
	}
	
	public boolean isArchived() {
		return archived;
	}
	
	public void setArchived(boolean archived) {
		this.archived = archived;
	}
	
	public ArrayList<Member> getVolunteers() {
		ArrayList<Member> volunteers = new ArrayList<Member>();
		for(String email : this.volunteers)
			volunteers.add(Database.getMember(email));
		return volunteers;
	}
	
	public void addVolunteer(String volunteer) {
		this.volunteers.add(volunteer);
	}
	
	public void removeVolunteer(Member volunteer) {
		this.volunteers.remove(volunteer.getEmail());
		volunteer.removeEvent(this.uuid);
	}
	
	public String toString(){
		return uuid + name + address + startDate + endDate + hours + info + archived;
	}
}
