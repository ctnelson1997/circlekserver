package us.oatbaron.circlek.objects;

import java.io.Serializable;

public class Email implements Serializable {
	private static final long serialVersionUID = -2316913654612591549L;
	
	public String address;
	public String subject;
	public String body;
}
